<?php

/**
 * Capsule UI entry point.
 *
 * @package capsule
 *
 * This file is part of the Capsule Theme for WordPress
 * https://crowdfavorite.com/capsule/
 *
 * Copyright (c) 2020 Crowd Favorite, Ltd. All rights reserved.
 * https://crowdfavorite.com
 *
 * **********************************************************************
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * **********************************************************************
 */

if (! isset($body_classes)) {
	$body_classes = array();
}

global $cap_client;
$cap_servers = $cap_client ? $cap_client->get_servers() : array();

$blog_desc         = get_bloginfo('description');
$title_description = ( is_home() && ! empty($blog_desc) ? ' - ' . $blog_desc : '' );

$permalink_structure = get_option('permalink_structure');
$search_permastruct  = ( empty($permalink_structure) ) ? '0' : '1';

if (function_exists('cftf_is_filter') && cftf_is_filter()) {
	$body_classes[] = 'filters-on';
}

$theme_url = trailingslashit(get_template_directory_uri());

$title = '';

if (is_home() || is_front_page()) {
	$title = __('Home', 'capsule');
} elseif (function_exists('cftf_is_filter') && cftf_is_filter()) {
	$title = __('Filter', 'capsule');
} elseif (is_search()) {
	// Translators: %s is the search term.
	$title = sprintf(__('Search: %s', 'capsule'), esc_html(get_query_var('s')));
} elseif (is_tag()) {
	$term = get_queried_object();
	// Translators: %s is the taxonomy term name.
	$title = sprintf(__('#%s', 'capsule'), $term->name);
} elseif (is_tax('projects')) {
	$term = get_queried_object();
	// Translators: %s is the taxonomy term name.
	$title = sprintf(__('@%s', 'capsule'), $term->name);
} elseif (is_tax('code')) {
	$term = get_queried_object();
	// Translators: %s is the taxonomy term name.
	$title = sprintf(__('`%s', 'capsule'), $term->name);
} elseif (is_tax('code')) {
	$term = get_queried_object();
	// Translators: %s is the taxonomy term name.
	$title = sprintf(__('`%s', 'capsule'), $term->name);
} elseif (is_author()) {
	$author = get_queried_object();
	// Translators: %s is the author name.
	$title = sprintf(__('Author: %s', 'capsule'), $author->display_name);
}
$title = apply_filters('capsule_page_title', $title);
?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width" />

	<title>
	<?php
		wp_title('|', true, 'right');
		echo esc_html(get_bloginfo('name'), 1) . esc_html($title_description);
	?>
	</title>

	<link rel="icon" href="<?php echo esc_url($theme_url); ?>ui/assets/icon/capsule-16.png" sizes="16x16">
	<link rel="icon" href="<?php echo esc_url($theme_url); ?>ui/assets/icon/capsule-32.png" sizes="32x32">
	<link rel="icon" href="<?php echo esc_url($theme_url); ?>ui/assets/icon/capsule-48.png" sizes="48x48">
	<link rel="icon" href="<?php echo esc_url($theme_url); ?>ui/assets/icon/capsule-128.png" sizes="128x128">
	<link rel="icon" href="<?php echo esc_url($theme_url); ?>ui/assets/icon/capsule-256.png" sizes="256x256">

	<?php wp_head(); ?>
</head>
<body <?php body_class(implode(' ', $body_classes)); ?>>
<div class="container">
	<nav class="main-nav">
		<ul>
			<?php do_action('capsule_main_nav_before'); ?>
			<li>
				<a
					href="<?php echo esc_url(home_url('/')); ?>"
					class="home icon capsule-icon-home"
					title="<?php esc_attr_e('Home', 'capsule'); ?>"
				>
					<span class="sr-only"><?php esc_html_e('Home', 'capsule'); ?></span>
				</a>
			</li>
			<li>
				<a href="https://gitlab.com/dashboard/snippets" target="_blank" rel="noopener noreferrer" title="GitLab">
					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAX8AAAF/CAMAAACWmjlVAAAAM1BMVEX////////////////////////////////////////////////////////////////////lEOhHAAAAEHRSTlMA8DAQ0KDAQGCA4CCQUHCw+BUOAQAACLtJREFUeAHs0YNhRQEAwMBv4+0/bW0zxWWE3Oi3tl8vNsN548VuPx19Y1rOx8PdjntXvqvVZnikzcqZ72iyGJ5otnTny1sPz7Tz52ubHodnm009+sr9s+GFZluXkv1XjSc+BfsBBPsBBPsBZPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBtPt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7APt7AP8BAAAgAAAEAIAABAEAIAAABACAAAD4N38BpPtncwBNV28BpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsBpPsB1PsB9PsB9PsB9PsB9Ps/DnDC3n0gWYoDURR9QgaE+7n/1c6Eb28yWnGJRG8FXed28W2J1s7ee21lBvDx+wOkun1ps96fGcDB7wjws+WtAAHC8fsDSGsLEIDn9wfQWgIE4Pn9AdRngC/4gQDLHiAAz29W5+HdJL8/gGqgAH7+GQDlRwMUOADPzwbIOxqA56cDLAYG4Pn5ADcZgOfnAxQwAM/PB1gNDMDz8wEaGIDn5wOsBgSIyG92ybMGBAjJn7I824wLwPPzDwDZgAAB+e2Sbx8gQEB+y9xrMD4Az1/k3GJAgHD8dso7AwKE47cu7woQIBy/rfKuGReA5+f9TwMCROO3Rd51AwJE4zdx/nwAnt8y588H4PltBf39AXh+3v9jQIBo/LZxzz/5ADy/dXmXDAgQjd8+cu4wAwJE47ck5y7jAvD8/AuwakCAePx2y7fduAA8P/8BQDIuAM/PX4AWIEBEfquaAUB+s2MGIPmtagYA+c2WGYDkt6YZAOQ3u2cAkt9smQFIfkt5BgD5zcoMwPDPADz/DMDzm+1LgAA0/3wayvP7144IAXh+/84cIADP71+qOUAAnt+/VXED+Pl5/yVAAD8/79+2AAH8/Ly/RQjg5+f9IwTw8/P+EQL4+Xn/CAH8/Lx/hAB+ft4/QgA/P+8fIYCfn/ePEMDPz/tHCODn5/0jBPDz8/4RAvj5ef8IAfz8vH+EAH5+3j9CAD8/7x8hgJ+f948QwM/P+wcIkIsF8H9iAIAf8H9wAIAf8H9wAIAf8PcFKLVv6/+7+tmAAAP4AX9ngHau+mrruQMBcrGI/rbJsaUOCQDwo/7+ADrOEQEAftbfH0BHGxAA4If9/QG0pQEBAH7Y3x9gKQMCAPywvz9AbgMCAPyAvyMAcDtbgB/wf1gAgB/w9wcY7JOOEef08v6jAqSRf8vcLYz/qADrwBN1FovjPyzAOe5Y5RbIf1iAvI86UWezQP7jAmyjrkB7JP+BAfYx91VcLZK/IwD8C1Df55/k2phHgPQ+//qUp0BVuux9/stTbueZpP4+/yQ95wLU3uf/eczbcHZrf5//LT3lJUCVvc9/1XPeBHqjf37O/ZzTG/31HH97of8u8n5607+F8/dv+ifjNv3LgKSBr//F/vGaTuM2n/+cuozbfP6/KRu3+fr3kD6v8+9y7h5xY8vLqM33P+9Xfv6eHnM78/zG75+YXXJtGXRXxTI//2UuP4fjYT2Cf8qP+A7u6fhgP4S/3U/49GvPjtfVMfx3ObaP+yFyeZe/df7J/+04giKMf8r01b9KQADa3/8a7DOAHwjA+vu/g3sN4AcD8P5pAW+cVCU2AO9vJWN//1gdkcP5W8kcPx+A97eSOX4+AO9vJXP8fADe39JKPPQCAWh//wvhbsP4+QC8v5VVv9haRvLzAXh/s/bTAsfHBvPzAXh/s7bpB7uaAfxAANzfLNXt0BfLV00G8AMBUP8vt7fe73W9e2+7mQH8QADAH1qVuADTv0pcgOlfJS7A9K8SF2D6V4kLMP2rxAWY/lXiAkz/KnEBpn+VuADTv0pcgOlfJS7A9K8SF2D6V4kLMP2rxAWY/lXiAkz/KnEBpn+VuADTv0pcgOlfJS7A9K8SF2D6V4kLMP2rxAWY/lUKHWDv2eEP8IcMkLrk+MImwB8ywCfrV1t3nj9wgHTpN8snzR84QDkcd0wG+IMGaNn7L+T5+QDxv79dpcABmuNfCPBHDVAydlInz88HSAt3Wg/Pzwe4gKPKaX4+gP+8niMB/GEDpEN/uw7whw3QkfPyeH4+gP/Azg3gDxrglGcJ4I8Z4JBnHeAPGaDItQPgDxnglm8F4I8Y4JBvJ8AfMMAu51aAP2CAj7wD+AMG6PJuB/jjBbjkXQP44wVY5d0J8vMBeP8O8McLIMSf5+cD8P48Px9gAf15fj7A6vcH+fkAvH8D+OMF6PKugPx8AP7njcDPByhybgH4IwbI8u0G+CMG2OTbh+PnA/A/dA7CzwfI8mwD+fkA/AWoAPwxA+xybAX5+QD8L0AD+KMG2DP435/n5wN08rNfnp8PsIBvffL8fICSwasPz88HqOBfv1RpBujs/XpngO3p/DPA5OcfAw6AP1wA/wkcVxrHPwOky3H8D8AfNoC1Qz/fto/mnwGsHg59gD9sAGtb1rc7+m6T3xHAt09fv7C/zmI2+V0B/Ev/tUcXBw5AARBCiUzc+q927bTufyJQAm/x1OlfCgjQKyCA+8cDuF+AgADuHw/gfgECArh/PID7BQgI4P7xAO4XICCA+8cDuF+AgADuHw/gfgECArh/PID7BQjcKID7BQgIcKP7BQgIcKP7BQgIcKP7BQjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjFBAjfzLZz91ebza9ivwChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmAChmADhPxNgcfqwA/+brU/vt1ny39l0cXqn3YwB2fJNgWwZlE0zOT1rfpgxNAkOWTwizBe71ZYL7Q7H02m600+YOAAAAABJRU5ErkJggg==" style="width: 23px;"/>
				</a>
			</li>
			<li>
				<a
					href="<?php echo esc_url(admin_url('post-new.php')); ?>"
					class="post-new-link icon capsule-icon-plus-circle"
					title="<?php esc_attr_e('Post New', 'capsule'); ?>"
				>
					<span class="sr-only"><?php esc_html_e('Post New', 'capsule'); ?></span>
				</a>
			</li>
			<li>
				<a href="#projects" class="projects" title="<?php esc_attr_e('Projects', 'capsule'); ?>">
					@
					<span class="sr-only"><?php esc_html_e('Projects', 'capsule'); ?></span>
				</a>
			</li>
			<li>
				<a
					href="#tags"
					class="tags icon capsule-icon-numbersign"
					title="<?php esc_attr_e('Tags', 'capsule'); ?>"
				>
					<span class="sr-only"><?php esc_html_e('Tags', 'capsule'); ?></span>
				</a>
			</li>
			<?php if (! empty($cap_servers)) : ?>
			<li>
				<a
					href="#servers"
					class="servers icon capsule-icon-globe"
					title="<?php esc_attr_e('Servers', 'capsule'); ?>"
				>
					<span class="sr-only"><?php esc_html_e('Servers', 'capsule'); ?></span>
				</a>
			</li>
			<?php endif; ?>

			<li>
				<a
					href="<?php echo esc_url(admin_url('admin.php?page=capsule')); ?>"
					class="icon capsule-icon-cog-wheel"
					title="<?php esc_attr_e('Capsule Help', 'capsule'); ?>"
				>
					<span class="sr-only"><?php esc_html_e('Capsule Help', 'capsule'); ?></span>
				</a>
			</li>
			<?php do_action('capsule_main_nav_after'); ?>
			<li><span class="spacer"></span></li>
		</ul>
	</nav>

	<div id="wrap">
		<header id="header">
			<div class="inner">
				<h1><?php echo esc_html($title); ?></h1>
				<form
					class="search clearfix"
					action="<?php echo esc_url(home_url('/')); ?>"
					method="get"
					data-permastruct="<?php echo esc_attr($search_permastruct); ?>"
				>
					<a href="#" class="filter-toggle"><?php esc_html_e('Filters', 'capsule'); ?></a>
					<input
						type="text"
						class="js-search"
						name="s"
						value="<?php echo esc_attr(get_query_var('s')); ?>"
						placeholder="<?php esc_attr_e('Search @projects, #tags, `code, etc&hellip;', 'capsule'); ?>"
					>
					<input type="submit" value="<?php esc_attr_e('Search', 'capsule'); ?>">
				</form>
			</div>
			<div class="filter clearfix">
			<?php capsule_taxonomy_filter(); ?>
			</div>
		</header>
		<div class="body">
		<?php if (have_posts()) : ?>
			<?php
			while (have_posts()) :
				the_post();

				if (is_singular()) {
					include 'views/content.php';
				} else {
					include 'views/excerpt.php';
				}
			endwhile;
			?>
			<?php if ($wp_query->max_num_pages > 1) : ?>
			<nav class="pagination clearfix">
				<div class="nav-previous">
					<?php next_posts_link(__('Older posts <span class="meta-nav">&rarr;</span>', 'capsule')); ?>
				</div>
				<div class="nav-next">
					<?php previous_posts_link(__('<span class="meta-nav">&larr;</span> Newer posts', 'capsule')); ?>
				</div>
			</nav>
			<?php endif; ?>
		<?php elseif (is_search()) : ?>
			<p class="search-no-results-msg">
				<?php esc_html_e('Nothing to see here&hellip; move along.', 'capsule'); ?>
			</p>
		<?php endif; ?>
		</div>
	</div>
</div>

<footer>
	<p>
		<a href="https://crowdfavorite.com/capsule/">Capsule</a> by
		<a href="https://crowdfavorite.com">Crowd Favorite</a> &middot; <?php wp_loginout(home_url()); ?>
	</p>
</footer>

<div id="projects">
	<h2><?php esc_html_e('Projects', 'capsule'); ?></h2>
	<ul>
	<?php
		wp_list_categories(
			array(
				'show_option_none' => '<span class="none">' . __('(none)', 'capsule') . '</none>',
				'taxonomy'         => 'projects',
				'title_li'         => '',
			)
		);
		?>
	</ul>
</div>
<div id="tags">
	<h2><?php esc_html_e('Tags', 'capsule'); ?></h2>
	<ul>
	<?php
		wp_list_categories(
			array(
				'show_option_none' => '<span class="none">' . __('(none)', 'capsule') . '</none>',
				'taxonomy'         => 'post_tag',
				'title_li'         => '',
			)
		);
		?>
	</ul>
</div>
<div id="servers">
	<h2><?php esc_html_e('Capsule Servers', 'capsule'); ?></h2>
	<ul>
	<?php
		// Don't handle 0 server situations here because the menu item is hidden in that situation.
		// $cap_servers set at top of page.
	?>
	<?php foreach ($cap_servers as $cap_server) : ?>
		<?php $server = $cap_client->process_server($cap_server); ?>
		<li>
			<a href="<?php echo esc_url($server->url); ?>" target="_blank">
				<i class="capsule-icon-open-in-new"></i>
				<?php echo esc_html($server->post_title); ?>
			</a>
		</li>
	<?php endforeach; ?>
	</ul>
</div>
<div class="connection-error"><?php esc_html_e('Lost connection to server.', 'capsule'); ?></div>

<?php if (! is_capsule_server() && ! current_user_can('unfiltered_html')) : ?>
	<div class="permissions-error">
		<?php esc_html_e('Capsule requires the unfiltered_html capability to work as expected.', 'capsule'); ?>
	</div>
<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>
